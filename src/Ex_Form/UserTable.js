import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DELETE_USER,LAY_SEARCHKEY,SHOW_THONG_TIN, TIM_KIEM_SV } from './redux/reducer/constRedux';
import SearchForm from './SearchForm';

class UserTable extends Component {
    renderListSv = () => {
        console.log(this.props.listSv);
        return this.props.listSv.map((item,key)=>{
            return <tr key={key}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.phone}</td>
                <td>{item.email}</td>
                <td><button onClick={()=>this.props.handleDeleteUser(item.id)} className='btn btn-danger'>Delete</button>
                <button onClick={() => this.props.showThongTinLenForm(item)} className='btn btn-warning'>Edit</button>
                </td>
            </tr>
        })
    }
    renderSearchForm = () => {
        if(this.props.visible == 1){
            return <SearchForm></SearchForm>
        }
    }
  render() {
    return (
      <div>
            <h4 className='text-left'>Tìm Kiếm Sinh Viên</h4>
            <div className="form-group d-flex">
                <input onChange={this.props.handleGetSearchForm} type="text" className="form-control" name value={this.props.searchKey} />
                <button onClick={this.props.handleTimSv} className='btn btn-success'><i class="fa fa-search"></i></button>
            </div>
                {this.renderSearchForm()}
            <table className="table">
                <thead className='bg-dark text-light'>
                    <tr>
                        <th>Mã SV</th>
                        <th>Họ Tên</th>
                        <th>Số Điện Thoại</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderListSv()}
                </tbody>
            </table>
            
      </div>
    )
  }
}
let mapDispatchToState = (dispatch) => {
    return {
        handleDeleteUser: (userId) => {
            let action  = {
                type: DELETE_USER,
                payload: userId,
            }
            dispatch(action);
        },
        showThongTinLenForm: (user) => {
            let action = {
                type: SHOW_THONG_TIN,
                payload: user,
            }
            dispatch(action);
        },
        handleTimSv: (searchKey) => {
            let action = {
                type: TIM_KIEM_SV,
                payload: searchKey,
            }
            dispatch(action);
        },
        handleGetSearchForm: (e) => {
            let action = {
                type: LAY_SEARCHKEY,
                payload: e,
            }
            dispatch(action);
        }
        ,
    }
}
let mapPropsToState = (state) => {
    return {
        listSv: state.formReducer.listSv,
        visible: state.formReducer.visible,
        searchKey: state.formReducer.searchKey,
    }
}
export default connect(mapPropsToState,mapDispatchToState)(UserTable);
