import { formReducer } from "./formReducer";
import {combineReducers} from "redux";

export const rootReducer = combineReducers({
    formReducer,
})