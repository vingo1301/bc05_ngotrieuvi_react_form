import { ADD_USER, CAP_NHAT_SV, DELETE_USER, LAY_SEARCHKEY, SHOW_KY_TU, SHOW_THONG_TIN, TIM_KIEM_SV } from "./constRedux";

let initialState = {
    listSv : [],
    sv:{
        id: "",
        name: "",
        phone: "",
        email: "",
    },
    valid:{
        isValid: [0,0,0,0],
        id: "",
        name: "",
        phone: "",
        email: "",
        resultAdd: "",
    },
    visible: 0,
    searchKey:"",
    searchList:[],
}
export const formReducer = (state = initialState,action) => {
    switch (action.type){
        case SHOW_KY_TU: {
            console.log(action.payload.target);
            let regex = "";
            let erMess = "";
            let {value,name:key} = action.payload.target;
            let cloneSv = {...state.sv};
            cloneSv[key] = value;
            let cloneValid = {...state.valid};
            let number = 0;
            if(key === "id"){
                regex = /^\d+$/;
                erMess = "Vui Lòng Chỉ Nhập Số!";
                number = 0;
            } else if(key==="name"){
                regex = /^[a-zA-Z\s]*$/;
                erMess = "Vui Lòng Chỉ Nhập Chữ và Khoảng Trắng!";
                number = 1;
            } else if(key==="phone"){
                regex = /^\d+$/;
                erMess = "Vui Lòng Chỉ Nhập Số!";
                number = 2;
            }
            else{
                regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                erMess = "Email Phải Có Định Dạng abc@xyz";
                number = 3;
            }
            let isRegex = regex.test(cloneSv[key]);
            
            if(isRegex == 0){
                cloneValid[key] = erMess;
                cloneValid.isValid[number] = 0;
            }
            else{
                cloneValid[key] = "";
                cloneValid.isValid[number] = 1;
            }
            console.log(cloneValid.isValid);
            return {...state,sv:cloneSv,valid:cloneValid};
        }
        case ADD_USER: {
            let cloneValid = {...state.valid};
            let cloneListSv = [...state.listSv];
            let cloneSv = {...state.sv};
            let number = -1;
            for (let key in state.sv ){
                number++;
                if(state.sv[key].length == 0){
                    cloneValid[key] = "Vui Lòng Không Để Trống!";
                    cloneValid.isValid[number] =0;
                    cloneValid.resultAdd = "";
                }
            }
            let kiemtraTrung = cloneListSv.findIndex((item) => {
                return item.id == cloneSv.id;
            })
            if (kiemtraTrung !== -1){ 
                cloneValid.isValid[0] = 0;
                cloneValid.id = "ID Đã Tồn Tại!"
            }
            let isAdd = 1;
            cloneValid.isValid.map((item) => {
                return isAdd &= item;
            })
            console.log(isAdd);
            if( isAdd == 1){
                cloneListSv.push(state.sv);
                cloneValid.resultAdd = "Thêm SV Thành Công!";
                cloneSv = {
                    id: "",
                    name: "",
                    phone: "",
                    email: "",
                };
            }
            
            return {...state,listSv:cloneListSv,sv:cloneSv,valid:cloneValid}
        }
        case DELETE_USER: {
            let cloneListSv = [...state.listSv];
            let index = cloneListSv.findIndex((item) => {
                return item.id == action.payload;
            });
            cloneListSv.splice(index,1);
            let cloneValid = {...state.valid};
            cloneValid.resultAdd = "Xoá SV thành công! "
            return {...state,listSv:cloneListSv,valid:cloneValid};
        }
        case SHOW_THONG_TIN: {
            return{...state,sv:action.payload}
        }
        case CAP_NHAT_SV: {
            let cloneValid = {...state.valid};
            let cloneListSv = [...state.listSv];
            let cloneSv = {...state.sv};
            let number = -1;
            for (let key in state.sv ){
                number++;
                if(state.sv[key].length == 0){
                    cloneValid[key] = "Vui Lòng Không Để Trống!";
                    cloneValid.isValid[number] =0;
                    cloneValid.resultAdd = "";
                }
            }
            let kiemtraTrung = cloneListSv.findIndex((item) => {
                return item.id == cloneSv.id;
            })
            if (kiemtraTrung == -1){ 
                cloneValid.isValid[0] = 0;
                cloneValid.id = "ID Không Tồn Tại!"
            }
            let isAdd = 1;
            cloneValid.isValid.map((item) => {
                return isAdd &= item;
            })
            if( isAdd == 1){
                cloneListSv[kiemtraTrung] = {...cloneSv};
                cloneValid.resultAdd = "Update SV Thành Công!";
                cloneSv = {
                    id: "",
                    name: "",
                    phone: "",
                    email: "",
                };
            }
            return {...state,listSv:cloneListSv,sv:cloneSv,valid:cloneValid}
        }
        case TIM_KIEM_SV: {
            
            let searchKey = state.searchKey;
            let cloneSearchList = [];
            state.listSv.forEach((item) => {
                for (let key in item){
                    if(item[key] == searchKey){
                        cloneSearchList.push(item);
                    }
                }
            })
            return {...state,visible:1,searchList:cloneSearchList}
        }
        case LAY_SEARCHKEY:{
            let cloneSearchKey = action.payload.target.value;
            return{...state,searchKey:cloneSearchKey}
        }
        default: return state;
    }
}