import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DELETE_USER, SHOW_THONG_TIN } from './redux/reducer/constRedux';

class SearchForm extends Component {
    renderSearchList = () => {
        console.log("searchList",this.props.searchList);
        return this.props.searchList.map((item) => {
            return <tr>
                <td>
                    {item.id}
                </td>
                <td>
                    {item.name}
                </td>
                <td>
                    {item.phone}
                </td>
                <td>
                    {item.email}
                </td>
            </tr>
        })
    }
  render() {
    return (
      <div><table className="table"><thead className='bg-dark text-light'>
      <tr>
          <th>Mã SV</th>
          <th>Họ Tên</th>
          <th>Số Điện Thoại</th>
          <th>Email</th>
      </tr>
         </thead>
        <tbody>
            {this.renderSearchList()}
        </tbody>
        </table>
    </div>

    )
  }
}
 let mapStateToProps = (state) => {
    return{
        searchList: state.formReducer.searchList,
    }
 }
 let mapDispatchToProps = (dispatch => {
    return {
        handleDeleteUser: (userId) => {
            let action  = {
                type: DELETE_USER,
                payload: userId,
            }
            dispatch(action);
        },
        showThongTinLenForm: (user) => {
            let action = {
                type: SHOW_THONG_TIN,
                payload: user,
            }
            dispatch(action);
        },
    }
 })
 export default connect(mapStateToProps,mapDispatchToProps)(SearchForm)
