
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADD_USER, CAP_NHAT_SV, LAY_SEARCHKEY, SHOW_KY_TU } from './redux/reducer/constRedux';
import UserTable from './UserTable';

class UserForm extends Component {
    
  render() {
    let {id,name,phone,email} = this.props.user;
    let {id:validId,name:validName,phone:validPhone,email:validEmail,resultAdd} = this.props.valid;
    return (
      <div className=''>
            <div className='text-left bg-dark text-light py-2 pl-2'>Thông tin sinh viên</div>
            <div className='row'>
            <div className="form-group col-6 text-left">
                <p className='mb-0'>Mã Sinh Viên:</p>
                <input onChange={this.props.handleGetUserForm} type="text" className="form-control" name="id" placeholder="Nhập Mã Sinh Viên" value={id} />
                <span className='text-danger'>{validId}</span>
            </div>
            <div className="form-group col-6 text-left">
                <p className='mb-0'>Họ Và Tên:</p>
                <input onChange={this.props.handleGetUserForm} type="text" className="form-control" name="name" placeholder = "Nhập Họ Tên" value={name} />
                <span className='text-danger'>{validName}</span>
            </div>
            </div>
            <div className='row'>
            <div className="form-group col-6 text-left">
                <p className='mb-0'>Số Điện Thoại:</p>
                <input onChange={this.props.handleGetUserForm} type="text" className="form-control" name="phone" placeholder="Nhập Số Điện Thoại" value={phone} />
                <span className='text-danger'>{validPhone}</span>
            </div>
            <div className="form-group col-6 text-left">
                <p className='mb-0'>Email:</p>
                <input onChange={this.props.handleGetUserForm} type="text" className="form-control" name="email" placeholder = "Nhập Email" value={email} />
                <span className='text-danger'>{validEmail}</span>
            </div>
            </div>
            <p className='text-center text-success'>{resultAdd}</p>
            <button onClick={this.props.handleAddUser} className='btn btn-success mb-2 mx-2'> Thêm Sinh Viên</button>
            <button onClick={this.props.handleUpdateUser} className='btn btn-primary mb-2'>Cập Nhật</button>
            <UserTable></UserTable>
      </div>
    )
  }
}
const mapPropsToState = (state) => {
    return {
        user: state.formReducer.sv,
        valid: state.formReducer.valid,
    }
}
const mapDispatchToState = (dispatch) => {
    return{
        handleGetUserForm: (e) => {
            let action = {
                type: SHOW_KY_TU,
                payload: e,
            }
            dispatch(action);
        },
        handleAddUser: (user) => {
            let action = {
                type: ADD_USER,
                payload: user,
            }
            dispatch(action);
        },
        handleUpdateUser: (user) => {
            let action = {
                type: CAP_NHAT_SV,
                payload: user,
            }
            dispatch(action);
        }
    }
}
export default connect(mapPropsToState,mapDispatchToState)(UserForm);
